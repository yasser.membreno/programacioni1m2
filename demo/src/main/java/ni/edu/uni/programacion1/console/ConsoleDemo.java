/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ni.edu.uni.programacion1.data.EmpleadoData;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleDemo {
    public static void start() throws IOException{
        BufferedReader reader = new BufferedReader(
        new InputStreamReader(System.in));
        int opc = 0;
        boolean flag = true;
        
        do{
            ConsoleMenus.mainMenu();            
            opc = ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc){
                case 1:
                    gestionEmpleado(reader);
                    break;
                case 2:
                    reportesEmpleado(reader);
                    break;
                case 3:
                    flag = false;
                    System.out.println("Hasta la vista ...!");
                    break;
                default:
                    System.err.println("Opcion no valida.");
            }
                    
        }while(flag);
    }
    
    public static void gestionEmpleado(BufferedReader reader) throws IOException{
        int opc = 0;
        boolean flag = true;
        EmpleadoData edata = new EmpleadoData();
        do{
            ConsoleMenus.gestionSubMenu();
            opc = ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc){
                case 1:
                    ConsoleGestionEmpleado.agregarEmpleado(reader, edata);
                    break;
                case 2:
                case 3:
                case 4:
                default:
            }
        }while(flag);
    }
    
    public static void reportesEmpleado(BufferedReader reader){
        
    }
    
}
