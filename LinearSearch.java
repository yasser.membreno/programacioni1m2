public class LinearSearch{
    public static void main(String[] args) {
        int edades[] = {33,25,11,8,15,22,7,14,35,1,17,19,19,45};
        int pos = -1, value = 52225555;

        pos = linearSearch(edades, value);
        
        System.out.format("%20s %20s\n","Valor","Posicion");
        //System.out.println("Valor\t Posicion");
        if(pos == -1){
            System.out.format("%20d %20d\n",value,pos);
            //System.out.println(value + "\t " + pos);
            return;
        }

        System.out.format("%20d %20d\n",value,(pos+1));
        //System.out.println(value + "\t " + pos);
    }

    public static int linearSearch(int array[], int value){
        int pos = -1;
        for(int i = 0; i < array.length; i++){
            if(value == array[i]){
                pos = i;
                break;
            }
        }

        return pos;
    }


}