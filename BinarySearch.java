import java.util.Arrays;
public class BinarySearch{
    public static void main(String[] args) {
        int edades[] = {33,25,11,8,15,22,7,14,35,1,17,19,19,45};
        int numbers[] = {4,38,3,11,500,50,9,17,18,12,55,40,15,7};
        int pos[] = new int[2], value = 15;

        Arrays.sort(edades);
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(edades));
        System.out.println(Arrays.toString(numbers));
        pos[0] = runBinarySearchIteratively(edades, value, 0, edades.length - 1);
        pos[1] = runBinarySearchIteratively(numbers, value, 0, numbers.length - 1);
       
        System.out.format("%20s %20s\n","Valor","Posicion");
        //System.out.println("Valor\t Posicion");
        for(int i = 0; i < pos.length; i++){
            if(pos[i] < 0){
                System.out.format("%20d %20d\n",value,pos[i]);
                //System.out.println(value + "\t " + pos);
                continue;
            }    
            System.out.format("%20d %20d\n",value,(pos[i]+1));
        }
        
        //System.out.println(value + "\t " + pos);

    }

    public static int runBinarySearchIteratively(int[] sortedArray, int key, 
                                          int low, int high) {
        int index = Integer.MAX_VALUE;
        
        while (low <= high) {
            int mid = (low + high) / 2;
            if (sortedArray[mid] < key) {
                low = mid + 1;
            } else if (sortedArray[mid] > key) {
                high = mid - 1;
            } else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
        return index;
    }

    public int runBinarySearchRecursively(
        int[] sortedArray, int key, int low, int high) {
        int middle = (low + high) / 2;
            
        if (high < low) {
            return -1;
        }
    
        if (key == sortedArray[middle]) {
            return middle;
        } else if (key < sortedArray[middle]) {
            return runBinarySearchRecursively(
            sortedArray, key, low, middle - 1);
        } else {
            return runBinarySearchRecursively(
            sortedArray, key, middle + 1, high);
        }
    }
}