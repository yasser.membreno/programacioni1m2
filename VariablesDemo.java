public class VariablesDemo{
    public static void main(String[] args) {
        Marcador m = new Marcador();

        System.out.println("Color: " + m1.getColor());
        System.out.println("Marca: " + Marcador.MARCA);
        
        Marcador.MARCA = "Artline";
        Marcador m1 = new Marcador();

        System.out.println("Color: " + m1.getColor());
        System.out.println("Marca: " + Marcador.MARCA);
    }
}